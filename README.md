# openpgp-proof

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:AEEF8CDB5ADF2F28016F39E1FBFC237DAF98D402]
